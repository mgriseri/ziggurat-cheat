#!/usr/bin/env python3

"""
A cheat script for Ziggurat.
It has been tested on Linux (openSUSE Leap 42.3),
with a game copy from gog.com.
"""

import argparse
import os
import re
import shutil
import sys
import xml.etree.ElementTree

USER_HOME = os.path.expanduser("~")
SCRIPT_NAME = sys.argv[0]
SCRIPT_CACHE_NAME = 'ziggurat-cheat-cache.txt'
SAVE_FILE_NAME = 'savegame.xml'
BACKUP_FILE_NAME = 'savegame.xml.b'
UNLOCK_FILE_NAME = 'unlock_data.xml'
BACKUP_UNLOCK_FILE_NAME = 'unlock_data.xml.b'


def find_savefile():
    """
    Search for a file named 'savegame.xml',
    with 'ziggurat' in its path.

    If found, it returns the directory of the file.
    Otherwise, it returns False.
    """
    savefile_found = False

    try:
        with open(SCRIPT_CACHE_NAME, 'r') as cached_savefile_dir:
            search_start = cached_savefile_dir.read()
    except FileNotFoundError:
        print('Locating your save file. Please wait...')
        search_start = USER_HOME  # scan entire home if no cache

    for root, dirs, files in os.walk(search_start):
        for name in files:
            if name == SAVE_FILE_NAME:
                savefile_path = os.path.join(root, name)
                savefile_dir = os.path.dirname(savefile_path)
                if 'ziggurat' in savefile_path.lower():
                    savefile_found = True
                    break
                break

    if savefile_found:
        # write save file location to a file
        # and return it
        with open(SCRIPT_CACHE_NAME, 'w') as cached_savefile_dir:
            cached_savefile_dir.write(savefile_dir)
        return savefile_dir
    else:
        return False


# CHEAT FUNCTIONS
def replace_in_save_file(regex, new_str):
    """
    Opens 'savegame.xml' file and substitute
    a Regex pattern with a new string.
    """
    print('Replacing content with %s' % (new_str))

    with open(SAVE_FILE_NAME) as savegame:
        savegame_content = savegame.read()

    pattern = re.compile(regex)
    tweaked_savegame = pattern.sub(new_str, savegame_content)

    with open(SAVE_FILE_NAME, 'w') as savegame:
        savegame.write(tweaked_savegame)


def unlock_everything():
    print('Applying unlock cheat...')
    with open(UNLOCK_FILE_NAME) as unlock_file:
        unlock_content = unlock_file.read()

    teaked_str = unlock_content.replace('false', 'true')

    with open(UNLOCK_FILE_NAME, 'w') as unlock_file:
        unlock_file.write(teaked_str)


def set_weapons(wand, spell, staff, alch):
    """
    Clears the weapon object in the save XML file
    and add element s to it using the arguments provided.
    Each of the 4 argument needs to be a valid weapon name.

    This function is supposed to be used by the set_weapons_ctrl function.
    """
    SAVEFILE_TREE = xml.etree.ElementTree.parse(SAVE_FILE_NAME)

    for elem in SAVEFILE_TREE.iter():
        if elem.tag == 'Player':
            player_obj = elem

    for elem in player_obj:
        if elem.tag == 'Weapons':
            weapon_obj = elem

    weapon_obj.clear()

    wand_elem = xml.etree.ElementTree.Element('Mana0')
    wand_elem.attrib = {'id': wand}
    weapon_obj.append(wand_elem)

    spell_elem = xml.etree.ElementTree.Element('Mana1')
    spell_elem.attrib = {'id': spell}
    weapon_obj.append(spell_elem)

    staff_elem = xml.etree.ElementTree.Element('Mana2')
    staff_elem.attrib = {'id': staff}
    weapon_obj.append(staff_elem)

    alch_elem = xml.etree.ElementTree.Element('Mana3')
    alch_elem.attrib = {'id': alch}
    weapon_obj.append(alch_elem)

    SAVEFILE_TREE.write(SAVE_FILE_NAME)


def set_weapons_ctrl():
    """
    Asks user which weapon he/she would like.
    Handles 4 different input by asking 4 different
    questions for each weapon.

    If the user enters a wrong input (not valid weapon choice),
    The programs exits.

    Uses the set_weapons function with the users's choices.
    """

    WANDS = [
        'WandDefault',  # should be just 'Wand'
        'WandFemaleApprentice',
        'WandAcrobat',
        'WandAlchemist',
        'WandBard',
        'WandHarlequin',
        'WandLibrarian',
        'WandNovice',
        'WandSeer',
        'WandShaman',
        'WandSorcerer',
        'WandTemplar',
        'WandVampire',
        'WandWitch',
        'WandBattlemage',
        'WandVagabond',
        'WandCleric',
    ]

    SPELLS = [
        ['SpellShotgun', 'Frozen Soul'],
        ['SpellFastShotgun', 'Hellish Amber'],
        ['SpellHomingGrenade', 'Skull Of Xanatos'],
        ['SpellPoison', 'Viper Fangs'],
        ['SpellHoming', 'Wraith Rings'],
        ['SpellFastHoming', 'Bloodlust Needles'],
        ['SpellBoomerang', "Archangel's Embrace"],
        ['SpellSphere', 'Whirlwind of Ulthar'],
        ['SpellTurret', 'The Lidless Eye'],
        ['SpellDivineRay', 'Arcane Storm'],
        ['SpellFlamethrower', 'Dragon Bile'],
        ['SpellSlowHoming', 'Enchanted Butterflies'],
    ]

    STAVES = [
        ['StaffSMG', 'Eye Of Twilight'],
        ['StaffWideShot', 'Undead Scepter'],
        ['StaffDoubleShot', 'Serpent Staff'],
        ['StaffRebound', 'Scarab Beetle Staff'],
        ['StaffSmartgun', 'Eagle Staff'],
        ['StaffTriangleShot', 'Staff Of Atlantis'],
        ['StaffRailgun', 'Solar Staff'],
        ['StaffTesla', "Nicola's Surge"],
        ['StaffChainLightning', 'Archon Staff'],
        ['StaffCross', 'Divine Wrath'],
        ['StaffReboundGround', 'Power Of Heart'],
        ['StaffSineShot', 'Gemini Staff'],
    ]

    ALCH_WEAPONS = [
        ['AlchemyGrenade', 'Firestorm Grenade'],
        ['AlchemyRocketLauncher', 'Magma Rifle'],
        ['AlchemyIncendiaryBomb', 'Fireweaver Bomb'],
        ['AlchemySpineGrenade', 'Porcupine Bomb'],
        ['AlchemyCrossbow', 'Frostbow'],
        ['AlchemyBigRocketLauncher', 'Magma Blaster'],
        ['AlchemyFireCluster', 'Flame Mortar'],
        ['AlchemyEnergySphere', 'Anguish Canon'],
        ['AlchemyElectricBomb', "Acheron's Cube"],
        ['AlchemyMiniGrenades', 'Catapult of Doom'],
        ['AlchemyBlackHole', 'Dark Cannon'],
        ['AlchemyFreezeBomb', 'Polar Blast'],
    ]

    # wand choice ctrl
    print('------- WANDS -------')
    for index, item in enumerate(WANDS):
        print(str(index).rjust(2), item[4::])

    print()
    print('Which wand would you like?')
    valid_inputs = [str(n) for n in range(len(WANDS))]
    wand_choice = str(input())
    if wand_choice not in valid_inputs:
        print('ERROR: Incorrect wand choice.')
        sys.exit()

    wand_choice = int(wand_choice)
    # handle when user wants default wand.
    if wand_choice == 0:
        wand_name = 'Wand'  # the valid name of default wand should be 'Wand'
    else:
        wand_name = WANDS[wand_choice]

    print()

    # spell choice ctrl
    print('------- SPELL BOOKS -------')
    for index, item in enumerate(SPELLS):
        print(str(index).rjust(2), item[1])

    print()
    print('Which spell book would you like?')

    valid_inputs = [str(n) for n in range(len(SPELLS))]
    spell_choice = str(input())
    if spell_choice not in valid_inputs:
        print('ERROR: Incorrect spell choice.')
        sys.exit()

    spell_choice = int(spell_choice)
    spell_name = SPELLS[spell_choice][0]

    print()

    # staff choice ctrl
    print('------- STAVES -------')
    for index, item in enumerate(STAVES):
        print(str(index).rjust(2), item[1])

    print()
    print('Which staff would you like?')

    valid_inputs = [str(n) for n in range(len(STAVES))]
    staff_choice = str(input())
    if staff_choice not in valid_inputs:
        print('ERROR: Incorrect staff choice.')
        sys.exit()

    staff_choice = int(staff_choice)
    staff_name = STAVES[staff_choice][0]

    print()

    # alchemy weapon choice ctrl
    print('------- ALCHEMY WEAPONS -------')
    for index, item in enumerate(ALCH_WEAPONS):
        print(str(index).rjust(2), item[1])

    print()
    print('Which alchemy weapon would you like?')

    valid_inputs = [str(n) for n in range(len(ALCH_WEAPONS))]
    alch_choice = str(input())
    if alch_choice not in valid_inputs:
        print('ERROR: Incorrect alchemy weapon choice.')
        sys.exit()

    alch_choice = int(alch_choice)
    alch_name = ALCH_WEAPONS[alch_choice][0]

    print()
    # use set_weapon function accordingly
    set_weapons(wand_name, spell_name, staff_name, alch_name)


# ARGUMENT HANDLING
parser = argparse.ArgumentParser(
    description='Script to cheat in Ziggurat')

parser.add_argument(
    '-H',
    '--health',
    action='store_true',
    help="fill health.")

parser.add_argument(
    '-m',
    '--mana',
    action='store_true',
    help="fill mana.")

parser.add_argument(
    '-k',
    '--key',
    action='store_true',
    help="get portal key.")

parser.add_argument(
    '-u',
    '--unlock',
    action='store_true',
    help="unlock everything.")

parser.add_argument(
    '-r',
    '--reveal',
    action='store_true',
    help="reveal map.")

parser.add_argument(
    '-w',
    '--weapons',
    action='store_true',
    help="set weapons.")

args = parser.parse_args()

# MAIN FUNCTION
if __name__ == '__main__':
    # search for the save file
    savefile_dir = find_savefile()
    print('Apply cheat in:')
    print(savefile_dir)
    print('')
    # run the script only if savefile_dir has been found.
    if savefile_dir:
        os.chdir(savefile_dir)
        cwd_files = os.listdir('.')
        if (BACKUP_FILE_NAME not in cwd_files) and (SAVE_FILE_NAME in cwd_files):
            shutil.copyfile(SAVE_FILE_NAME, BACKUP_FILE_NAME)
        if (BACKUP_UNLOCK_FILE_NAME not in cwd_files) and (UNLOCK_FILE_NAME in cwd_files):
            shutil.copyfile(UNLOCK_FILE_NAME, BACKUP_UNLOCK_FILE_NAME)
    else:
        print("ERROR: Save file cannot be found.")
        sys.exit()

    if args.health:
        health_regex = r'Health max=".*" value=".*"'
        health_newstr = 'Health max="9999" value="9999"'
        replace_in_save_file(health_regex, health_newstr)
        print('Done!')

    elif args.mana:
        mana0_regex = r'Mana0 max=".*" value=".*"'
        mana0_newstr = 'Mana0 max="9999" value="9999"'
        replace_in_save_file(mana0_regex, mana0_newstr)

        mana1_regex = r'Mana1 max=".*" value=".*"'
        mana1_newstr = 'Mana1 max="9999" value="9999"'
        replace_in_save_file(mana1_regex, mana1_newstr)

        mana2_regex = r'Mana2 max=".*" value=".*"'
        mana2_newstr = 'Mana2 max="9999" value="9999"'
        replace_in_save_file(mana2_regex, mana2_newstr)

        mana3_regex = r'Mana3 max=".*" value=".*"'
        mana3_newstr = 'Mana3 max="9999" value="9999"'
        replace_in_save_file(mana3_regex, mana3_newstr)
        print('Done!')

    elif args.key:
        replace_in_save_file(r'hasKey=".*"', 'hasKey="true"')
        print('Done!')

    elif args.unlock:
        unlock_everything()
        print('Done!')

    elif args.reveal:
        replace_in_save_file(r'visible=".*"', 'visible="true"')
        print('Done!')

    elif args.weapons:
        set_weapons_ctrl()
        print('Done!')

    else:
        print("No cheat enabled. Read help first:")
        print("$ python3 %s --help" % (SCRIPT_NAME))
