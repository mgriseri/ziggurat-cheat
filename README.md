# Ziggurat save file editor

Ziggurat-Cheat is a command-line save file editor for the game **Ziggurat** written in Python.

It has 6 features: 

- Maximum health

- Maximum mana

- Get portal key

- Unlock everything

- Reveal map

- Weapon selector

### INSTALL

First, make sure you have **python3** installed.
Then, use the following commands to download the script and install it.

    wget "https://gitlab.com/mgriseri/ziggurat-cheat/raw/master/ziggurat-cheat.py"
    chmod +x ziggurat-cheat.py
    sudo mv ziggurat-cheat.py /usr/local/bin/ziggurat-cheat

> You can uninstall it by removing the file with `sudo rm /usr/local/bin/ziggurat-cheat`

### USAGE

When the game is **NOT** running, use one of these commands from a terminal:

    ziggurat-cheat --health
    ziggurat-cheat --mana
    ziggurat-cheat --key
    ziggurat-cheat --unlock
    ziggurat-cheat --reveal
    ziggurat-cheat --weapons


### LICENSE

GNU GENERAL PUBLIC LICENSE:

https://www.gnu.org/licenses/gpl.html

### CONTRIBUTE

If you want to adapt the source code, it is under the GPL, you're free to do so. 

If this script has been helpful to you, feel free to [send a donation](https://www.paypal.me/mgriseri).